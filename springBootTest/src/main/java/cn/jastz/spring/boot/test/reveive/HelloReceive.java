package cn.jastz.spring.boot.test.reveive;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhiwen
 */
@Component
public class HelloReceive {

    @RabbitListener(queues = "myqueue")
    public void processHelloMsg(String content) {
        System.out.println("consumer1" + content);
        throw new RuntimeException();
    }

    @RabbitListener(queues = "myqueue")
    public void processHelloMsg1(String content) {
        System.out.println("consumer2" + content);
    }
}
