package cn.jastz.spring.boot.test.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author zhiwen
 */
@RestController
public class SendHelloMsgController {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping("sendMsg2Hello/{msg}")
    public ResponseEntity<Boolean> sendMsgToHelloTopic(@PathVariable("msg") String msg) throws InterruptedException {
        ResponseEntity<Boolean> responseEntity;
        final CountDownLatch confirmLatch = new CountDownLatch(1);
        String id = UUID.randomUUID().toString().replace("-", "");
        if (rabbitTemplate.isConfirmListener()) {
            rabbitTemplate.setConfirmCallback(null);
        }
        rabbitTemplate.setConfirmCallback(((correlationData, ack, cause) -> {
            if (Objects.equals(correlationData.getId(), id)) {
                if (ack) {
                    confirmLatch.countDown();
                } else {
                    System.out.println("发送失败原因：" + cause);

                }
            }
        }));
        rabbitTemplate.convertAndSend("hello", (Object) msg, new CorrelationData(id));
        if (confirmLatch.await(10, TimeUnit.SECONDS)) {
            System.out.println("消息发送成功");
            responseEntity = new ResponseEntity<>(true, HttpStatus.OK);
        } else {
            System.out.println("消息发送失败");
            responseEntity = new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
