package cn.jastz.java.classLoadCondition;

/**
 * @author zhiwen
 * Sun HotSpot VM Option:-XX:+TraceClassLoading 用来查看类加载
 */
public class Main {
    public static void main(String[] args) {
//        System.out.println(SubClass.a);
//        System.out.println(SubClass.b);
//        System.out.println(FinalClass.a);
//        System.out.println(FinalClass.class.getAnnotations());
//        System.out.println(System.getProperty("java.class.path"));
//        System.out.println(SuperClass.class);
    }
}
