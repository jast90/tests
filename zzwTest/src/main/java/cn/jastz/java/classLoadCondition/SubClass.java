package cn.jastz.java.classLoadCondition;

/**
 * @author zhiwen
 */
public class SubClass extends SuperClass {
    static {
        System.out.println("Sub class load");
    }
    public static int v = 1;
}
