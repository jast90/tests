package cn.jastz.java.classLoadCondition;

/**
 * @author zhiwen
 */
public class SuperClass {

    static {
        System.out.println("SuperClass is load");
    }

    /**
     * 常量是是在编译时进行初始化，放入常量池的
     */
    public final static int a = 1;
    public static int b = 1;


    public static void hello(){
        System.out.println("Hello super class");
    }

}
