package cn.jastz.java.classLoadCondition;

/**
 * @author zhiwen
 */
public final class FinalClass {
    static {
        System.out.println("Final class load in compile.");
    }

    public static final int a = 10;
}
