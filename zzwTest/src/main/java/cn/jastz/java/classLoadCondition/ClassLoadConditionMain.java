package cn.jastz.java.classLoadCondition;

public class ClassLoadConditionMain {
    /**
     * 通过 VM Option:-XX:+TraceClassLoading 用来查看类加载
     *
     * @param args
     */
    public static void main(String[] args) {
//        newJVMClassCmdCondition();
//        getStaticJVMClassCmdCondition();
//        invokeStaticJVMClassCmdCondition();
//        reflectCondition();
//        subClassLoadCondition();
        mainClassCondition();
    }

    /**
     * 1. new 字节码指令
     */
    public static void newJVMClassCmdCondition() {
        new SuperClass();
    }

    /**
     * 2.访问、设置static变量
     */
    public static void getStaticJVMClassCmdCondition() {
//        System.out.println(SuperClass.a);访问final修饰的static变量是不加载类的
        System.out.println(SuperClass.b);
    }

    /**
     * 3. 访问静态方法
     */
    public static void invokeStaticJVMClassCmdCondition() {
//        System.out.println(SuperClass.a);访问final修饰的static变量是不加载类的
        SuperClass.hello();
    }

    /**
     * 4. 通过java.lang.reflect访问
     * 输出：
     * [Loaded cn.jastz.java.classLoadCondition.SuperClass from file:/Users/zhangzhiwen/IdeaProjects/xxx-test/zzwTest/target/classes/]
     * [Loaded cn.jastz.java.classLoadCondition.SubClass from file:/Users/zhangzhiwen/IdeaProjects/xxx-test/zzwTest/target/classes/]
     */
    public static void reflectCondition() {
        System.out.println(SuperClass.class);
    }

    /**
     * 5.子类加载时，优先加载父类
     */
    public static void subClassLoadCondition() {
//        new SubClass();
        System.out.println(SubClass.v);
    }

    /**
     * 6. 虚拟机先加载主类
     * 输出：
     * [Loaded cn.jastz.java.classLoadCondition.ClassLoadConditionMain from file:/Users/zhangzhiwen/IdeaProjects/xxx-test/zzwTest/target/classes/]
     * <p>
     * <p>
     * 始终都会加载主类
     */
    public static void mainClassCondition() {

    }

    /**
     *
     * 7.遇到REF_getStatic、REF_putStatic、REF_invokeStatic的方法句柄，优先加载这些句柄对应的类
     * TODO：
     */
}
