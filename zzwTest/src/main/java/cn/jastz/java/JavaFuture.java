package cn.jastz.java;

import java.util.concurrent.*;

/**
 * @author zhiwen
 */
public class JavaFuture {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Future<String> f = executorService.submit(() -> {
            Thread.sleep(1000 * 3);
            return "success";
        });
        executorService.shutdown();
        System.out.println(f.get());
        /**
         * 阻塞
         */
        System.out.println("Main thread is blocked.");
    }

}
