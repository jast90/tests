package cn.jastz.spring.data.mongodb.estore;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author zhiwen
 */
@Configuration
@EnableMongoRepositories
public class ApplicationConfig extends AbstractMongoConfiguration {


    @Override
    protected String getDatabaseName() {
        return "mongo-template";
    }


    @Override
    public MongoClient mongoClient() {
        return new MongoClient("192.168.99.100");
    }
}
