package cn.jastz.spring.data.mongodb.estore.entity;

import org.springframework.data.annotation.Id;

import java.math.BigInteger;

/**
 * @author zhiwen
 */
public class AbstractDocument {
    @Id
    private BigInteger id;


    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }
}
