package cn.jastz.spring.data.mongodb.estore.repository;

import cn.jastz.spring.data.mongodb.estore.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.Optional;

/**
 * @author zhiwen
 */
@Repository
public interface CustomerRepository extends MongoRepository<Customer, BigInteger> {

    Optional<Customer> findFirstByFirstName(String firstName);

}
