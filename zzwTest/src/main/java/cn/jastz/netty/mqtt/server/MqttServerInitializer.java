package cn.jastz.netty.mqtt.server;

import cn.jastz.netty.websocket.codec.ByteBufToWebSocketFrameEncoder;
import cn.jastz.netty.websocket.codec.WebSocketFrameToByteBufDecoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author zhiwen
 */
public class MqttServerInitializer extends ChannelInitializer<SocketChannel> {
    private final ChannelGroup group;

    public MqttServerInitializer(ChannelGroup group) {
        this.group = group;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(65536));
        pipeline.addLast(new WebSocketServerProtocolHandler("/mqtt", "mqtt,mqttv3.1", true));
//        pipeline.addLast(new WebSocketFrameHandler());
        pipeline.addLast(new WebSocketFrameToByteBufDecoder());
        pipeline.addLast(new ByteBufToWebSocketFrameEncoder());
        pipeline.addLast(new MqttDecoder());
        pipeline.addLast(MqttEncoder.INSTANCE);
        pipeline.addLast("timeout", new IdleStateHandler(30, 0, 20, TimeUnit.SECONDS));
        pipeline.addLast(new MqttServerHandler());

    }
}
