package cn.jastz.netty.mqtt.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.mqtt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.util.Date;

/**
 * @author zhiwen
 */
public class MqttServerHandler extends SimpleChannelInboundHandler<Object> {

    private IClientStore iClientStore;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (((MqttMessage) msg).decoderResult().isSuccess()) {
            MqttMessage mqttMessage = (MqttMessage) msg;
            switch (mqttMessage.fixedHeader().messageType()) {
                case CONNECT:
                    doConnectMessage(ctx, msg);
                    return;
                case PINGREQ:
                    doPingMessage(ctx, msg);
                    return;
                case DISCONNECT:
                    doDisConnectMessage(ctx, msg);
                    return;
                case PUBLISH:
                    // 收到客户端推送过来的消息（会包含主题），将消息保存到相关的主题，并通知订阅该主题的其他客户端（不包括自己）
                    // 如何实现将消息发送到订阅的已连接客户端？如果此次发送失败是否需要重试？
                    doPublishMessage(ctx, msg);
                    return;
                case SUBSCRIBE:
                    //处理客户端订阅主题消息，将该主题自客户端订阅以后产生的消息发送到该客户端
            }
        }
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Send greeting for a new connection
        System.out.println("Welcome to " + InetAddress.getLocalHost().getHostName());
        ctx.writeAndFlush("Welcome to " + InetAddress.getLocalHost().getHostName() + "!\r\n");
        ctx.writeAndFlush("It is " + new Date() + " now.\r\n");
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        logger.debug(ctx.channel().remoteAddress().toString().substring(1, ctx.channel().remoteAddress().toString().lastIndexOf(":")) + "is close!");

    }

    private void doConnectMessage(ChannelHandlerContext ctx, Object msg) {
        MqttConnectMessage connectMessage = (MqttConnectMessage) msg;
        String clientId = connectMessage.payload().clientIdentifier();
        logger.debug("client connect,client id is 【{}】", clientId);
        //TODO 用户连接，更新用户登入信息，以及处理用户未处理完的消息
        MqttConnAckVariableHeader variableheader = new MqttConnAckVariableHeader(MqttConnectReturnCode.CONNECTION_ACCEPTED, false);
        MqttConnAckMessage connAckMessage = new MqttConnAckMessage(new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0), variableheader);
        iClientStore.addClient(new Client(clientId, ctx));
        ctx.write(connAckMessage);
    }

    private void doPingMessage(ChannelHandlerContext ctx, Object msg) {
        logger.debug("ping response！");
        MqttFixedHeader header = new MqttFixedHeader(MqttMessageType.PINGRESP, false, MqttQoS.AT_MOST_ONCE, false, 0);
        MqttMessage pingRespMessage = new MqttMessage(header);
        ctx.write(pingRespMessage);
    }

    private void doDisConnectMessage(ChannelHandlerContext ctx, Object msg) {
        ctx.close();
    }

    private void doPublishMessage(ChannelHandlerContext ctx, Object msg) {
        MqttPublishMessage message = (MqttPublishMessage) msg;
        ByteBuf buf = message.payload();
        String msgStr = new String(ByteBufUtil.getBytes(buf));
        int msgId = message.variableHeader().packetId();
        if (msgId == -1) {
            msgId = 1;
        }
        String topicName = message.variableHeader().topicName();
        logger.debug("终端消息上报 start，终端编码为：{},主题：{}，消息体：{}", "", topicName, msgStr);
        if (message.fixedHeader().qosLevel() == MqttQoS.AT_MOST_ONCE) {
            MqttMessageIdVariableHeader header = MqttMessageIdVariableHeader.from(msgId);
            MqttPubAckMessage puback = new MqttPubAckMessage(new MqttFixedHeader(MqttMessageType.PUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                    header);
            ctx.write(puback);
        }
    }
}
