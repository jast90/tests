package cn.jastz.netty.mqtt.server;

import java.util.Queue;

/**
 * @author zhiwen
 */
public interface IMessageStore {
    Queue<Message> getMessageByTopic(String topic);

    void addMessageToTopic(String topic, Message message);
}
