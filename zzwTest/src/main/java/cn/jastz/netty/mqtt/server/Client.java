package cn.jastz.netty.mqtt.server;

import io.netty.channel.ChannelHandlerContext;

/**
 * @author zhiwen
 */
public class Client {
    private String clientId;
    private ChannelHandlerContext channelHandlerContext;

    public Client(String clientId, ChannelHandlerContext channelHandlerContext) {
        this.clientId = clientId;
        this.channelHandlerContext = channelHandlerContext;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public ChannelHandlerContext getChannelHandlerContext() {
        return channelHandlerContext;
    }

    public void setChannelHandlerContext(ChannelHandlerContext channelHandlerContext) {
        this.channelHandlerContext = channelHandlerContext;
    }
}
