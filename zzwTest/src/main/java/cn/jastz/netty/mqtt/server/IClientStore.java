package cn.jastz.netty.mqtt.server;

/**
 * @author zhiwen
 */
public interface IClientStore {

    boolean containClientId(String clientId);

    boolean addClient(Client client);

    Client getByClientId(String clientId);

    boolean removeClient(String clientId);
}
