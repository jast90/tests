package cn.jastz.netty.websocket;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author zhiwen
 */
public class WebSocketFrameHandler extends SimpleChannelInboundHandler<Object> {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (msg instanceof TextWebSocketFrame) {
            TextWebSocketFrame msg1 = (TextWebSocketFrame) msg;
            String request = msg1.text();
            logger.info("{} received {}"
                    , ctx.channel(), request);
            ctx.channel().write(new TextWebSocketFrame(request.toUpperCase()));
        }

        if (msg instanceof BinaryWebSocketFrame) {
            BinaryWebSocketFrame msg1 = (BinaryWebSocketFrame) msg;
            ByteBuf byteBuf = msg1.content();
            TextWebSocketFrame textWebSocketFrame = new TextWebSocketFrame(byteBuf);
            String request = textWebSocketFrame.text();
            logger.info("{} received {}"
                    , ctx.channel(), request);
            ctx.channel().write(new TextWebSocketFrame(request));
        }

    }


}
