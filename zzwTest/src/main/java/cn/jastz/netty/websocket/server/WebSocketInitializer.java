package cn.jastz.netty.websocket.server;

import cn.jastz.netty.websocket.WebSocketFrameHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @author zhiwen
 */
public class WebSocketInitializer extends ChannelInitializer<SocketChannel> {


    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(65536));
        pipeline.addLast("timeout", new IdleStateHandler(30, 0, 20, TimeUnit.SECONDS));
        pipeline.addLast(new WebSocketServerProtocolHandler("/mqtt", null, true));
        pipeline.addLast(new WebSocketFrameHandler());
    }
}
