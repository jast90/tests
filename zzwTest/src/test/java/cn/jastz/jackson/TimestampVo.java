package cn.jastz.jackson;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.sql.Timestamp;

/**
 * @author zhiwen
 */
public class TimestampVo {


    @JsonDeserialize(using = CustomerTimestampDeserialize.class)
    private Timestamp time;

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
