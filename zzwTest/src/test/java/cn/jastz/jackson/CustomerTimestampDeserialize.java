package cn.jastz.jackson;

/**
 * @author zhiwen
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 */
public class CustomerTimestampDeserialize extends JsonDeserializer<Timestamp> {

    private SimpleDateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss");

    @Override
    public Timestamp deserialize(JsonParser paramJsonParser,
                                 DeserializationContext paramDeserializationContext)
            throws IOException {
        String str = paramJsonParser.getText().trim();
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        try {
            Date d1 = dateFormat.parse(str);
            if (d1 != null) {
                return new Timestamp(d1.getTime());
            }
        } catch (ParseException e) {
            // Handle exception here
        }
        Date d1 = paramDeserializationContext.parseDate(str);
        if (d1 != null) {
            return new Timestamp(d1.getTime());
        } else {
            return null;
        }
    }
}
