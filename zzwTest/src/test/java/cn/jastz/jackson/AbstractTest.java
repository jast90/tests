package cn.jastz.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;

/**
 * @author zhiwen
 */
public class AbstractTest {
    protected ObjectMapper objectMapper;

    @Before
    public void before() {
        objectMapper = new ObjectMapper();
    }
}
