package cn.jastz.jackson;

import com.google.common.collect.Lists;
import me.jastz.common.json.JsonUtil;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhiwen
 */
public class JacksonSerializedTimestamp {

    @Test
    public void test() {
        TimestampVo timestampVo = new TimestampVo();
        timestampVo.setTime(new Timestamp(Calendar.getInstance().getTime().getTime()));
        String json = JsonUtil.objectToJson(timestampVo);
        System.out.println(json);
        System.out.println(JsonUtil.jsonToObject(json, TimestampVo.class).getTime());
    }


    @Test
    public void test1() {
        Timestamp timestamp = new Timestamp(1553610576000L);
        Timestamp timestamp1 = new Timestamp(1553567376000L);
        System.out.println(timestamp);
        System.out.println(timestamp1);
    }

    @Test
    public void filter() {
        List<String> stringList = Lists.newArrayList("123", null, "456");
        List<String> result = stringList.stream().filter(s -> s != null).collect(Collectors.toList());
        System.out.println(result);
    }

    @Test
    public void sort() {
        List<Integer> ints = Lists.newArrayList(1, 4, 2, 5, 8, 3);
        List<Integer> sortedInts = ints.stream().sorted(Integer::compareTo).collect(Collectors.toList());
        System.out.println(sortedInts);

    }
}
