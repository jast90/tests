package cn.jastz.jackson.enums;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author zhiwen
 */
@JsonSerialize(using = IResultSerializer.class)
public interface IResult {

    void setResultCode(int resultCode);

    int getResultCode();

    void setResultMsg(String resultMsg);

    String getResultMsg();
}
