package cn.jastz.jackson.enums;

/**
 * @author zhiwen
 */
public enum Result implements IResult {
    SUCCESS(0), ERROR(-1);
    private int resultCode;
    private String resultMsg;

    Result(int resultCode) {
        this.resultCode = resultCode;
    }

    Result(int resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public int getResultCode() {
        return this.resultCode;
    }

    @Override
    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultMsg() {
        return this.resultMsg;
    }

    @Override
    public String toString() {
        return this.name();
    }
}
