package cn.jastz.jackson.enums;

import cn.jastz.jackson.AbstractTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;

/**
 * @author zhiwen
 */
public class EnumTest extends AbstractTest {

    @Test
    public void defaultSerialize() {
        try {
            String ubh = objectMapper.writeValueAsString(DeviceType.UBH);
            System.out.println(ubh);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void resultTest() {
        try {
            String result = objectMapper.writeValueAsString(Result.SUCCESS);
            System.out.println(result);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
