package cn.jastz.proxy.cglib;

/**
 * @author zhiwen
 */
public class HelloService {

    public void hello() {
        System.out.println("hello world");
    }
}
