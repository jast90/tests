package cn.jastz.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;

/**
 * @author zhiwen
 */
public class HelloServiceClient {

    public static void main(String[] args) {
        Enhancer e = new Enhancer();
        //生成代理类继承的类
        e.setSuperclass(HelloService.class);
        //生成代理类实现的接口
//        e.setInterfaces(new Class[]{});
        e.setCallback(new ExecuteTimeMethodInterceptor());
        HelloService helloService = (HelloService) e.create();
        helloService.hello();
    }
}
