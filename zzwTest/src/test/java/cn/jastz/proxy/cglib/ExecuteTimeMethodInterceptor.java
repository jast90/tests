package cn.jastz.proxy.cglib;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author zhiwen
 */
public class ExecuteTimeMethodInterceptor implements MethodInterceptor {

    /**
     *
     * @param obj
     * @param method
     * @param args
     * @param proxy
     * @return
     * @throws Throwable
     */
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        long startTime = System.currentTimeMillis();
        System.out.println("start");
        Object result = proxy.invokeSuper(obj, args);
        System.out.println("end");
        long endTime = System.currentTimeMillis();
        System.out.println("execute time in millisecond is:" + (endTime - startTime));
        return result;
    }
}
