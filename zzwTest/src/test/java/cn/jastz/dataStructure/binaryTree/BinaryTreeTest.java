package cn.jastz.dataStructure.binaryTree;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zhiwen
 */
public class BinaryTreeTest {
    BinaryTree root = new BinaryTree(10, new BinaryTree(2, null, new BinaryTree(3, new BinaryTree(0, null, null), null)), new BinaryTree(5, new BinaryTree(22, new BinaryTree(21, new BinaryTree(18, null, new BinaryTree(16, null, null)), null), null), null));

    @Test
    public void depth() {
        Assert.assertTrue(BinaryTree.depth(root) == 6);
    }

    @Test
    public void isBalance() {
//        root = new BinaryTree(1, new BinaryTree(2, new BinaryTree(4, null, null), null), new BinaryTree(3, null, null));
        long start = System.currentTimeMillis();
        boolean is = BinaryTree.isBalance(root);
        System.out.println(String.format("第一种方法,是否是平衡树：%s,消耗时间%s ms", is, (System.currentTimeMillis() - start)));
        AtomicInteger rootDepth = new AtomicInteger(0);
        start = System.currentTimeMillis();
        is = BinaryTree.isBalance(root, rootDepth);
        System.out.println(String.format("第一种方法,是否是平衡树：%s,消耗时间%s ms，深度：%s", is, (System.currentTimeMillis() - start), rootDepth));
    }

    @Test
    public void intTest() {
        AtomicInteger a = new AtomicInteger(10);
        change(a);
        System.out.println(a.get());
    }

    private void change(AtomicInteger a) {
        a.set(20);
    }

}
