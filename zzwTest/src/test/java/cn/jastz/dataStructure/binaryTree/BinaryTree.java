package cn.jastz.dataStructure.binaryTree;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Java 版的二叉树
 *
 * @author zhiwen
 */
public class BinaryTree {

    private int value;
    private BinaryTree left;
    private BinaryTree right;

    public BinaryTree(int value, BinaryTree left, BinaryTree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public BinaryTree getLeft() {
        return left;
    }

    public void setLeft(BinaryTree left) {
        this.left = left;
    }

    public BinaryTree getRight() {
        return right;
    }

    public void setRight(BinaryTree right) {
        this.right = right;
    }

    /**
     * 计算二叉树的深度
     *
     * @param binaryTree
     * @return
     */
    public static int depth(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return 0;
        }
        int leftDepth = depth(binaryTree.left);
        int rightDepth = depth(binaryTree.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }

    /**
     * 是否是平衡二叉树
     *
     * @return
     */
    public static boolean isBalance(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return true;
        }

        int leftDepth = depth(binaryTree.left);
        int rightDepth = depth(binaryTree.right);
        int diff = Math.abs(leftDepth - rightDepth);
        if (diff > 1) {
            return false;
        }
        return isBalance(binaryTree.left) && isBalance(binaryTree.right);
    }

    /**
     * 是否是平衡二叉树（通过空间换时间）
     *
     * @param binaryTree
     * @param depth      为什么用AtomicInteger？因为这里需要的是一个可以改值的对象
     * @return
     */
    public static boolean isBalance(BinaryTree binaryTree, AtomicInteger depth) {
        if (binaryTree == null) {
            depth = new AtomicInteger(0);
            return true;
        }
        AtomicInteger leftDepth = new AtomicInteger(0), rightDepth = new AtomicInteger(0);
        if (isBalance(binaryTree.left, leftDepth) && isBalance(binaryTree.right, rightDepth)) {
            int diff = Math.abs(leftDepth.get() - rightDepth.get());
            if (diff > 1) {
                return false;
            }
            depth.set(1 + Math.max(leftDepth.get(), rightDepth.get()));
            return true;
        }
        return false;
    }
}
