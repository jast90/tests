package cn.jastz.locks.redis;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.UUID;

/**
 * @author zhiwen
 */
public class SimpleLock {

    private RedisTemplate<String, String> redisTemplate;

    public SimpleLock(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    /**
     * 获取锁及锁的值
     *
     * @param lockName
     * @param timeoutMS
     * @return
     */
    public String acquireLock(String lockName, long timeoutMS) {
        String lockValue = UUID.randomUUID().toString();
        long endTime = System.currentTimeMillis() + timeoutMS;
        while (System.currentTimeMillis() < endTime) {
            if (redisTemplate.opsForValue().setIfAbsent(getLockName(lockName), lockValue)) {
                return lockValue;
            }
        }
        return null;
    }

    public boolean releaseLock(String lockName, String lockValue) {
        try {
            redisTemplate.watch(getLockName(lockName));
            if (StringUtils.equals(redisTemplate.opsForValue().get(getLockName(lockName)), lockValue)) {
                redisTemplate.multi();
                redisTemplate.delete(getLockName(lockName));
                redisTemplate.exec();
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    private String getLockName(String lockName) {
        return String.format("lock:%s", lockName);
    }
}
