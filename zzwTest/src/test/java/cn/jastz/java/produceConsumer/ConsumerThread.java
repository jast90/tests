package cn.jastz.java.produceConsumer;

import java.util.Queue;

/**
 * @author zhiwen
 */
public class ConsumerThread extends Thread {
    private Queue<Integer> queue;

    public ConsumerThread(String name, Queue<Integer> queue) {
        super(name);
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (queue) {
                while (queue.isEmpty()) {
                    try {
                        queue.wait();
                        System.out.println("Buffer is empty,wait producer produce ...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                int i = queue.remove();
                System.out.println("Consuming value:" + i);
                queue.notifyAll();
            }
        }
    }
}
