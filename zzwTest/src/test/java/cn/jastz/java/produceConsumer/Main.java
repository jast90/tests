package cn.jastz.java.produceConsumer;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author zhiwen
 */
public class Main {

    public static void main(String[] args) {
        int maxSize = 20;
        Queue<Integer> queue = new LinkedList<>();
        Thread a = new ProducerThread("producer", queue, maxSize);
        Thread b = new ConsumerThread("consumer", queue);
        a.start();
        b.start();
    }
}
