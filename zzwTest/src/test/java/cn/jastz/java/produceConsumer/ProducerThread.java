package cn.jastz.java.produceConsumer;

import java.util.Queue;
import java.util.Random;

/**
 * @author zhiwen
 */
public class ProducerThread extends Thread {
    private Queue<Integer> buffer;
    private int maxSize;

    public ProducerThread(String name, Queue<Integer> buffer, int maxSize) {
        super(name);
        this.buffer = buffer;
        this.maxSize = maxSize;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (buffer) {
                while (buffer.size() == maxSize) {
                    try {
                        System.out.println(String.format("Queue is full,producer wait consumer take something from queue."));
                        buffer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Random random = new Random();
                int i = random.nextInt();
                System.out.println(String.format("Produce value:%s", i));
                buffer.add(i);
                buffer.notifyAll();
            }
        }
    }
}
