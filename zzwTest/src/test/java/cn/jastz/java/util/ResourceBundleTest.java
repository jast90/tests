package cn.jastz.java.util;

import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author zhiwen
 */
public class ResourceBundleTest {

    @Test
    public void test() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("i18n.i18n");
        System.out.println(resourceBundle.getString("hello"));
        Locale locale = Locale.US;
        resourceBundle = ResourceBundle.getBundle("i18n.i18n", locale);
        System.out.println(resourceBundle.getString("hello"));
    }
}
