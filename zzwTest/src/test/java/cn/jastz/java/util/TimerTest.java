package cn.jastz.java.util;

import org.junit.Test;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author zhiwen
 */
public class TimerTest {
    public static void main(String[] args) {
        Timer timer = new Timer();
        AtomicInteger counter = new AtomicInteger();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println(String.format("Task   实际运行时间：%s,需要运行时间：%s", new Date(), new Date(this.scheduledExecutionTime())));
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        TimerTask timerTask1 = new TimerTask() {
            @Override
            public void run() {

                System.out.println(String.format("Task 1 实际运行时间：%s,需要运行时间：%s,计时器：%s", new Date().getTime(), new Date(this.scheduledExecutionTime()), counter.addAndGet(1)));
            }
        };
        System.out.println("开始执行时间：" + new Date());
        timer.schedule(timerTask, 0, 1000);//timerTask运行时所需的时间>周期时间时，运行可以看出是按运行时间作为周期时间执行的。具体的原因 TODO 确认timerTask运行时所需的时间>周期时间时
        timer.scheduleAtFixedRate(timerTask1, 0, 1000);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void test() {
        long l = 1000;
        l >>= 1;
        System.out.println(l);
    }
}
