package cn.jastz.java.util;

import org.junit.Test;

import java.util.Locale;

/**
 * @author zhiwen
 */
public class LocaleTest {
    @Test
    public void test() {
        Locale locale1 = new Locale("zh", "cn");
        System.out.println(locale1);
        Locale locale2 = new Locale.Builder().setLanguage("zh").build();
        System.out.println(locale2);
        System.out.println(Locale.getDefault());
    }
}
