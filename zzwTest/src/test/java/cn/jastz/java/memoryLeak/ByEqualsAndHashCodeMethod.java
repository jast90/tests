package cn.jastz.java.memoryLeak;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashMap;

public class ByEqualsAndHashCodeMethod {

    public static void main(String[] args) {
        HashMap<Person, Integer> map = new HashMap<>();
        Person person1 = new Person("hello");
        Person person2 = new Person("hello");
        System.out.println(person1.hashCode() == person2.hashCode());
        for (int i = 0; i < 10000; i++) {
            map.put(new Person("hello"), 1);
        }
        System.out.println(map.size());
        for(;;);

    }
}

class Person {
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return new EqualsBuilder()
                .append(name, person.name)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name)
                .toHashCode();
    }

    /*@Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Person )) {
            return false;
        }
        Person person = (Person) o;
        return person.name.equals(name);
    }*/
}
