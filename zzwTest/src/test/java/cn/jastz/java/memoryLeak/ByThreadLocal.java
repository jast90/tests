package cn.jastz.java.memoryLeak;

import java.io.IOException;

public class ByThreadLocal {
    public static ThreadLocal<Integer[]> local = ThreadLocal.withInitial(() -> new Integer[0]);
    public static void set(Integer[] a){
        local.set(a);
    }

    public static void main(String[] args) throws IOException {
        System.in.read();
        for (int i=0;i<1000;i++){
            new Thread(()->{
                set(new Integer[100000]);
            }).start();
        }
        while (true);
    }
}
