package cn.jastz.java.memoryLeak;

import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;

/**
 * 类变量导致的内存泄漏
 * 按package-info.java中注释进行调试
 * 结果：
 * 说明类变量不能被GC
 */
public class ByStaticFeild {
    private static List<Double> list = Lists.newArrayList();
    private static void addElement(){
        for(int i = 0;i<10000000;i++){
            list.add(Math.random());
        }
        System.out.println("debug point 2");
    }

    public static void main(String[] args) throws IOException {
        System.out.println("debug point 1");
        System.in.read();//输入值之前堆内存大小大概在25MB左右
        addElement();//输入完后堆内存大小在360MB左右
        System.out.println("debug point 3");
        while(true){

        }
    }
}
