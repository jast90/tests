package cn.jastz.java.memoryLeak;

import java.io.*;

/**
 * 资源未关闭导致内存泄漏？实验表名GC可以回收堆内存，可以回收但是也会导致泄漏
 * 按package-info.java中注释进行调试
 * 结果：
 * 堆内存被回收了
 */
public class ByResourceNotClose {
    public static void main(String[] args) throws IOException {
        System.in.read();//堆内存大小为20MB左右
        for (int i = 0; i < 10000; i++) {
            byte[] b = new byte[1000000];
            InputStream is = null;
            try {
                is = new ByteArrayInputStream(b);
            } catch (Exception e) {
                /**/
            } finally {
                /*if(is!=null){
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }*/
            }
        }//打开很多资源时，堆内存为560MB，然后变到390MB，执行GC后，资源被回收变为16MB
        while (true) ;
    }
}
