/**
 * 调试如下：
 * 1.打开VisualVM
 * 2.运行如下代码
 * 3.在VisualVM中查看该进程
 * 4.选择monitor标签页，点击perform GC,堆中的内存是否被回收了，未回收说明存在泄漏
 */
package cn.jastz.java.memoryLeak;