package cn.jastz.java.thread.wait.io;

import java.io.IOException;

public class ThreadWaitByIO {
    public static void main(String[] args) {
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
