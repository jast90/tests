package cn.jastz.java.thread;

import java.util.HashSet;
import java.util.Set;

/**
 * 两个线程按字母顺序打印字母（a...z),一个线程打印元音字母（a,e,i,o,u)，一个线程打印辅音字母(26个去掉5个元音字母）
 * 输出结果：
 * Thread-0: a
 * Thread-1: b
 * Thread-1: c
 * Thread-1: d
 * Thread-0: e
 * Thread-1: f
 * Thread-1: g
 * Thread-1: h
 * Thread-0: i
 * Thread-1: j
 * Thread-1: k
 * Thread-1: l
 * Thread-1: m
 * Thread-1: n
 * Thread-0: o
 * Thread-1: p
 * Thread-1: q
 * Thread-1: r
 * Thread-1: s
 * Thread-1: t
 * Thread-0: u
 * Thread-1: v
 * Thread-1: w
 * Thread-1: x
 * Thread-1: y
 * Thread-1: z
 *
 * 结果分析：
 * 元音字母（a,e,i,o,u）都是Thread-0打印的
 * 辅音字母都是Thread-1打印的
 */
public class Print26LetterBy2Thread {

    public static void main(String[] args) {
        Util util = new Util();
        util.print();
    }



}

class Util {
    private char temp;
    private volatile boolean done = false;//26个字母遍历完成标志
    private Set<Character> characterSet = new HashSet<>();//防止重复打印
    private Object aLetterMonitor = new Object();//单个字母锁

    public char[] getLetter() {
        char a = 'a';
        char[] let = new char[26];
        for (int i = 0; i < 26; i++) {
            let[i] = (char) (a + i);
        }
        return let;
    }

    public boolean isVowel(char a) {
        if (a < 'a' && a > 'z') {
            throw new IllegalArgumentException();
        }
        switch (a) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                return true;
            default:
                return false;

        }
    }

    public void print() {
        char[] let = getLetter();
        /**
         * 监控打印元音字母
         */
        Thread t = new Thread(() -> {
            while (!done) {
                synchronized (aLetterMonitor) {
                    if (isVowel(temp) && !characterSet.contains(temp)) {
                        characterSet.add(temp);
                        System.out.println(String.format("%s: %s", Thread.currentThread().getName(), temp));
                        aLetterMonitor.notify();
                    }
                }
            }

        });
        t.start();

        /**
         * 遍历26个字母并打印辅音字母
         */
        Thread t2 = new Thread(() -> {
            for (char a : let) {
                synchronized (aLetterMonitor) {//TODO 是否可以改为join实现？
                    temp = a;
                    if (isVowel(temp)) {
                        try {
                            aLetterMonitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println(String.format("%s: %s", Thread.currentThread().getName(), temp));
                    }
                }
            }
            done = true;
        });
        t2.start();


    }
}