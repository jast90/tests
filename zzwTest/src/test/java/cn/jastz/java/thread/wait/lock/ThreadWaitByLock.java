package cn.jastz.java.thread.wait.lock;

import java.io.IOException;

public class ThreadWaitByLock {

    public static void main(String[] args) throws IOException {
        Object o = new Object();
        createLockThread(o);
        System.in.read();
        synchronized (o){
            o.notify();
        }
    }

    public static void createLockThread(final Object lock){
        new Thread(()->{
            synchronized(lock){
                try{
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"testLockThread").start();
    }
}
