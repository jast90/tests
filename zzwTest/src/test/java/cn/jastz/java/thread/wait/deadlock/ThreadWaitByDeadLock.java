package cn.jastz.java.thread.wait.deadlock;


public class ThreadWaitByDeadLock implements Runnable{

    private int a ;
    private int b;

    public ThreadWaitByDeadLock(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        synchronized (Integer.valueOf(a)){
            synchronized (Integer.valueOf(b)){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(a+b);
            }
        }
    }

    /**
     * 多线程死锁
     * 通过jstack 定位线程间死锁、也可以通过jconsole来检测死锁
     * 通过 jps 获得虚拟机唯一编码
     *
     * jstack -l 11403
     * 2019-10-09 19:29:46
     * Full thread dump Java HotSpot(TM) 64-Bit Server VM (25.191-b12 mixed mode):
     *
     * "Attach Listener" #31 daemon prio=9 os_prio=31 tid=0x00007fb22e8a9800 nid=0x330b waiting on condition [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "DestroyJavaVM" #30 prio=5 os_prio=31 tid=0x00007fb22e148000 nid=0x2703 waiting on condition [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-19" #29 prio=5 os_prio=31 tid=0x00007fb22e928000 nid=0x9803 waiting for monitor entry [0x0000700007b37000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:19)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	- locked <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-18" #28 prio=5 os_prio=31 tid=0x00007fb22e927000 nid=0x6103 waiting for monitor entry [0x0000700007a34000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:19)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	- locked <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-17" #27 prio=5 os_prio=31 tid=0x00007fb22f0d8000 nid=0x6003 waiting for monitor entry [0x0000700007931000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-16" #26 prio=5 os_prio=31 tid=0x00007fb22e926800 nid=0x9c03 waiting for monitor entry [0x000070000782e000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-15" #25 prio=5 os_prio=31 tid=0x00007fb22e925800 nid=0x9d03 waiting for monitor entry [0x000070000772b000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-14" #24 prio=5 os_prio=31 tid=0x00007fb22f0d7000 nid=0x5d03 waiting for monitor entry [0x0000700007628000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-13" #23 prio=5 os_prio=31 tid=0x00007fb22e925000 nid=0x5c03 waiting for monitor entry [0x0000700007525000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-12" #22 prio=5 os_prio=31 tid=0x00007fb22f06e000 nid=0x5b03 waiting for monitor entry [0x0000700007422000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-11" #21 prio=5 os_prio=31 tid=0x00007fb22e924000 nid=0xa103 waiting for monitor entry [0x000070000731f000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-10" #20 prio=5 os_prio=31 tid=0x00007fb22e923800 nid=0x5a03 waiting for monitor entry [0x000070000721c000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-9" #19 prio=5 os_prio=31 tid=0x00007fb22f06d000 nid=0xa403 waiting for monitor entry [0x0000700007119000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-8" #18 prio=5 os_prio=31 tid=0x00007fb22f06c800 nid=0x5903 waiting for monitor entry [0x0000700007016000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-7" #17 prio=5 os_prio=31 tid=0x00007fb22e922800 nid=0xa703 waiting for monitor entry [0x0000700006f13000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-6" #16 prio=5 os_prio=31 tid=0x00007fb22f06b800 nid=0xa803 waiting for monitor entry [0x0000700006e10000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-5" #15 prio=5 os_prio=31 tid=0x00007fb22e922000 nid=0xa903 waiting for monitor entry [0x0000700006d0d000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-4" #14 prio=5 os_prio=31 tid=0x00007fb22e921000 nid=0x5503 waiting for monitor entry [0x0000700006c0a000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-3" #13 prio=5 os_prio=31 tid=0x00007fb22e82f800 nid=0x4303 waiting for monitor entry [0x0000700006b07000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-2" #12 prio=5 os_prio=31 tid=0x00007fb22f06b000 nid=0x4503 waiting for monitor entry [0x0000700006a04000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Thread-1" #11 prio=5 os_prio=31 tid=0x00007fb22feaa000 nid=0x4603 waiting for monitor entry [0x0000700006901000]
     *    java.lang.Thread.State: BLOCKED (on object monitor)
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:17)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Service Thread" #9 daemon prio=9 os_prio=31 tid=0x00007fb22e0bb800 nid=0x3c03 runnable [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "C1 CompilerThread2" #8 daemon prio=9 os_prio=31 tid=0x00007fb22e896800 nid=0x3a03 waiting on condition [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "C2 CompilerThread1" #7 daemon prio=9 os_prio=31 tid=0x00007fb22e0ba800 nid=0x4903 waiting on condition [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "C2 CompilerThread0" #6 daemon prio=9 os_prio=31 tid=0x00007fb22e0ba000 nid=0x3903 waiting on condition [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Monitor Ctrl-Break" #5 daemon prio=5 os_prio=31 tid=0x00007fb22e018000 nid=0x3703 runnable [0x00007000061ec000]
     *    java.lang.Thread.State: RUNNABLE
     * 	at java.net.SocketInputStream.socketRead0(Native Method)
     * 	at java.net.SocketInputStream.socketRead(SocketInputStream.java:116)
     * 	at java.net.SocketInputStream.read(SocketInputStream.java:171)
     * 	at java.net.SocketInputStream.read(SocketInputStream.java:141)
     * 	at sun.nio.cs.StreamDecoder.readBytes(StreamDecoder.java:284)
     * 	at sun.nio.cs.StreamDecoder.implRead(StreamDecoder.java:326)
     * 	at sun.nio.cs.StreamDecoder.read(StreamDecoder.java:178)
     * 	- locked <0x00000007959b8090> (a java.io.InputStreamReader)
     * 	at java.io.InputStreamReader.read(InputStreamReader.java:184)
     * 	at java.io.BufferedReader.fill(BufferedReader.java:161)
     * 	at java.io.BufferedReader.readLine(BufferedReader.java:324)
     * 	- locked <0x00000007959b8090> (a java.io.InputStreamReader)
     * 	at java.io.BufferedReader.readLine(BufferedReader.java:389)
     * 	at com.intellij.rt.execution.application.AppMainV2$1.run(AppMainV2.java:64)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Signal Dispatcher" #4 daemon prio=9 os_prio=31 tid=0x00007fb22f825000 nid=0x3603 runnable [0x0000000000000000]
     *    java.lang.Thread.State: RUNNABLE
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Finalizer" #3 daemon prio=8 os_prio=31 tid=0x00007fb22f849800 nid=0x5103 in Object.wait() [0x0000700005fe6000]
     *    java.lang.Thread.State: WAITING (on object monitor)
     * 	at java.lang.Object.wait(Native Method)
     * 	- waiting on <0x0000000795588ed0> (a java.lang.ref.ReferenceQueue$Lock)
     * 	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
     * 	- locked <0x0000000795588ed0> (a java.lang.ref.ReferenceQueue$Lock)
     * 	at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
     * 	at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:216)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "Reference Handler" #2 daemon prio=10 os_prio=31 tid=0x00007fb22f849000 nid=0x5203 in Object.wait() [0x0000700005ee3000]
     *    java.lang.Thread.State: WAITING (on object monitor)
     * 	at java.lang.Object.wait(Native Method)
     * 	- waiting on <0x0000000795586bf8> (a java.lang.ref.Reference$Lock)
     * 	at java.lang.Object.wait(Object.java:502)
     * 	at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
     * 	- locked <0x0000000795586bf8> (a java.lang.ref.Reference$Lock)
     * 	at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)
     *
     *    Locked ownable synchronizers:
     * 	- None
     *
     * "VM Thread" os_prio=31 tid=0x00007fb22f846000 nid=0x2d03 runnable
     *
     * "GC task thread#0 (ParallelGC)" os_prio=31 tid=0x00007fb22e807800 nid=0x1e07 runnable
     *
     * "GC task thread#1 (ParallelGC)" os_prio=31 tid=0x00007fb22f80d800 nid=0x1f03 runnable
     *
     * "GC task thread#2 (ParallelGC)" os_prio=31 tid=0x00007fb22f80e000 nid=0x5403 runnable
     *
     * "GC task thread#3 (ParallelGC)" os_prio=31 tid=0x00007fb22f80e800 nid=0x2c03 runnable
     *
     * "VM Periodic Task Thread" os_prio=31 tid=0x00007fb22e0e6000 nid=0x3d03 waiting on condition
     *
     * JNI global references: 15
     *
     *
     * Found one Java-level deadlock:
     * =============================
     * "Thread-19":
     *   waiting to lock monitor 0x00007fb22e91ca08 (object 0x00000007955bb8b0, a java.lang.Integer),
     *   which is held by "Thread-18"
     * "Thread-18":
     *   waiting to lock monitor 0x00007fb22e823358 (object 0x00000007955bb8c0, a java.lang.Integer),
     *   which is held by "Thread-19"
     *
     * Java stack information for the threads listed above:
     * ===================================================
     * "Thread-19":
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:19)
     * 	- waiting to lock <0x00000007955bb8b0> (a java.lang.Integer)
     * 	- locked <0x00000007955bb8c0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     * "Thread-18":
     * 	at cn.jastz.java.thread.wait.deadlock.ThreadWaitByDeadLock.run(ThreadWaitByDeadLock.java:19)
     * 	- waiting to lock <0x00000007955bb8c0> (a java.lang.Integer)
     * 	- locked <0x00000007955bb8b0> (a java.lang.Integer)
     * 	at java.lang.Thread.run(Thread.java:748)
     *
     * Found 1 deadlock.
     *
     * @param args
     */
    public static void main(String[] args) {
        for (int i=0;i<10;i++){
            new Thread(new ThreadWaitByDeadLock(1,2)).start();
            new Thread(new ThreadWaitByDeadLock(2,1)).start();
        }

    }
}
