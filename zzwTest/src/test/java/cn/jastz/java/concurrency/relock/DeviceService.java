package cn.jastz.java.concurrency.relock;

import com.google.common.util.concurrent.Striped;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author zhiwen
 */
public class DeviceService {
    private static final Striped<Lock> striped = Striped.lazyWeakLock(127);
    //TODO 后期可以提取一个通用的锁
    private static ConcurrentHashMap<Integer, ReentrantLock> deviceLockMap = new ConcurrentHashMap<>();

    public void openDevice1(Device device) {
        Lock lock = striped.get(device);
        try {
            lock.lock();
            System.out.println(String.format("%s,开始开门", Thread.currentThread().getName()));
            System.out.println(String.format("%s,开门...", Thread.currentThread().getName()));
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println(String.format("%s,结束开门", Thread.currentThread().getName()));
            lock.unlock();
        }
    }


    public void openDevice(Device device) {
        Integer key = device.getDeviceId();

        ReentrantLock lock = new ReentrantLock();
        ReentrantLock temp;
        lock = (temp = deviceLockMap.putIfAbsent(key, lock)) != null ? temp : lock;

        /*
        //这种写法是非线程安全的，参考：https://stackoverflow.com/questions/14947723/is-concurrenthashmap-totally-safe
        //会使得获取的锁不一致，从而导致执行顺序错误
        if (deviceLockMap.containsKey(key)) {
            lock = deviceLockMap.get(key);
        } else {
            lock = new ReentrantLock();
            deviceLockMap.putIfAbsent(key, lock);
        }
        */

        String deviceThread = String.format("deviceId:%s,%s", device.getDeviceId(), Thread.currentThread().getName());
        try {
            lock.lock();
            System.out.println(String.format("%s,lock:%s", deviceThread, lock));
            System.out.println(String.format("%s,开始开门", deviceThread));
            System.out.println(String.format("%s,开门...", deviceThread));
            Thread.sleep(1000);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println(String.format("%s,结束开门", deviceThread));
            System.out.println(String.format("%s,排队线程数：%s", deviceThread, lock.getQueueLength()));
            System.out.println();
            lock.unlock();
            if (!lock.hasQueuedThreads()) {
                deviceLockMap.remove(device.getDeviceId());
            }
        }
    }

}
