package cn.jastz.java.concurrency.relock;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhiwen
 */
public class GuavaStripedTest {
    private DeviceService stripedService = new DeviceService();

    @Test
    public void testStriped1() {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (int i = 20; i > 0; i--) {
            Device device = new Device();

            if (i % 2 == 0) {
                device.setDeviceId(1000199);
            } else {
                device.setDeviceId(1000198);
            }
            device.setDeviceId(1000199);
            executorService.submit(() -> stripedService.openDevice(device));
//            executorService.submit(() -> stripedService.openDevice1(device));
        }
        executorService.shutdown();
        while (true) {
            if (executorService.isTerminated()) {
                break;
            }
        }

    }
    /* 运行结果1：
开始开门
开始开门
pool-1-thread-2cn.jastz.java.concurrency.relock.Device@6dee96
pool-1-thread-1cn.jastz.java.concurrency.relock.Device@5fe95859
结束开门
排队线程数：0
没有排队线程，移除锁
结束开门
排队线程数：18
开始开门
pool-1-thread-3cn.jastz.java.concurrency.relock.Device@4d5e672c
结束开门
排队线程数：17
开始开门
pool-1-thread-4cn.jastz.java.concurrency.relock.Device@15d7d5ef
结束开门
排队线程数：16
开始开门
pool-1-thread-5cn.jastz.java.concurrency.relock.Device@2830b820
结束开门
排队线程数：15
开始开门
pool-1-thread-6cn.jastz.java.concurrency.relock.Device@33fadc8f
结束开门
排队线程数：14
开始开门
pool-1-thread-7cn.jastz.java.concurrency.relock.Device@9728c99
结束开门
排队线程数：13
开始开门
pool-1-thread-9cn.jastz.java.concurrency.relock.Device@653764dc
结束开门
排队线程数：12
开始开门
pool-1-thread-8cn.jastz.java.concurrency.relock.Device@37cb82a8
结束开门
排队线程数：11
开始开门
pool-1-thread-10cn.jastz.java.concurrency.relock.Device@1d4e383b
结束开门
排队线程数：10
开始开门
pool-1-thread-12cn.jastz.java.concurrency.relock.Device@13a505be
结束开门
排队线程数：9
开始开门
pool-1-thread-11cn.jastz.java.concurrency.relock.Device@460d3c91
结束开门
排队线程数：8
开始开门
pool-1-thread-13cn.jastz.java.concurrency.relock.Device@741193f2
结束开门
排队线程数：7
开始开门
pool-1-thread-14cn.jastz.java.concurrency.relock.Device@47ab7c3e
结束开门
排队线程数：6
开始开门
pool-1-thread-15cn.jastz.java.concurrency.relock.Device@33ab55bc
结束开门
排队线程数：5
开始开门
pool-1-thread-16cn.jastz.java.concurrency.relock.Device@618133ad
结束开门
排队线程数：4
开始开门
pool-1-thread-17cn.jastz.java.concurrency.relock.Device@7eaffc1f
结束开门
排队线程数：3
开始开门
pool-1-thread-18cn.jastz.java.concurrency.relock.Device@2e6b8dfb
结束开门
排队线程数：2
开始开门
pool-1-thread-19cn.jastz.java.concurrency.relock.Device@57cb40c4
结束开门
排队线程数：1
开始开门
pool-1-thread-20cn.jastz.java.concurrency.relock.Device@5a119359
结束开门
排队线程数：0
没有排队线程，移除锁

Process finished with exit code 0
     */
}
