package cn.jastz.java.concurrency.relock;

import java.io.Serializable;

/**
 * @author zhiwen
 */
public class Device implements Serializable {
    private int deviceId;

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }
}
