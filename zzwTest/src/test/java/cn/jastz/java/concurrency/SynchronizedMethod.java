package cn.jastz.java.concurrency;

public class SynchronizedMethod {

    public synchronized void m1(){
        System.out.println("m1 ....");
    }

    public synchronized void m2(){
        System.out.println("m2 ....");
        try {
            //为了更好验证类同步方法的锁 锁是类还是对象
            Thread.sleep(1000*30);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
