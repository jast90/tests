package cn.jastz.java.concurrency;

import org.junit.Test;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author zhiwen
 */
public class CopyOnWriteArrayListsTest {

    @Test
    public void read() {
        CopyOnWriteArrayList<String> copyOnWriteArrayList = new CopyOnWriteArrayList("Hello,World".split(","));
        new Thread(() -> System.out.println(copyOnWriteArrayList.get(0))).start();
        copyOnWriteArrayList.add(0, "hello1");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
