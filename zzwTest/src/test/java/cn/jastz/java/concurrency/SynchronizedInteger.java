package cn.jastz.java.concurrency;

/**
 * @author zhiwen
 */
public class SynchronizedInteger {
    private int i = 0;

    public synchronized void increase() {
        i++;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("线程：%s,+1;value:%s", Thread.currentThread().getName(), i));
    }

    public synchronized void decrease() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        i--;
        System.out.println(String.format("线程：%s,-1 ;value:%s", Thread.currentThread().getName(), i));
    }

    public synchronized int getValue() {
        System.out.println(String.format("线程：%s,get value:%s", Thread.currentThread().getName(), i));
        return i;
    }
}
