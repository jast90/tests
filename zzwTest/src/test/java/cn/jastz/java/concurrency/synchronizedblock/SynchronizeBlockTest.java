package cn.jastz.java.concurrency.synchronizedblock;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhiwen
 */
public class SynchronizeBlockTest {
    SynchronizeBlock synchronizeBlock = new SynchronizeBlock();

    @Test
    public void test() {
        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (int i = 0; i < 10; i++) {
            pool.submit(() -> {
                String idStr = "10";//
                int id = 10;
//                synchronizeBlock.open("" + idStr);//这样是无法实现同步的，执行会混乱
                synchronizeBlock.open(idStr);//可以正确同步
//                synchronizeBlock.open(id);//不同步
//                synchronizeBlock.openByLock(id);//同步
            });
        }
        pool.shutdown();
        while (true) {
            if (pool.isTerminated()) {
                break;
            }
        }
    }
}
