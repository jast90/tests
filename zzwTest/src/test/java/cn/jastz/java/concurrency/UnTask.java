package cn.jastz.java.concurrency;

/**
 * @author zhiwen
 */
public class UnTask implements Runnable {

    private UnSynchronizedInteger unsynchronizedInteger;

    public UnTask(UnSynchronizedInteger unsynchronizedInteger) {
        this.unsynchronizedInteger = unsynchronizedInteger;
    }

    @Override
    public void run() {
        unsynchronizedInteger.increase();
    }
}
