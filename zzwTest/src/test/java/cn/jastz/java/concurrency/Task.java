package cn.jastz.java.concurrency;

/**
 * @author zhiwen
 */
public class Task implements Runnable {

    private SynchronizedInteger synchronizedInteger;

    public Task(SynchronizedInteger synchronizedInteger) {
        this.synchronizedInteger = synchronizedInteger;
    }

    @Override
    public void run() {
        synchronizedInteger.increase();
    }
}
