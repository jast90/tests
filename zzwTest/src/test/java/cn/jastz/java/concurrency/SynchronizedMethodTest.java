package cn.jastz.java.concurrency;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhiwen
 */
public class SynchronizedMethodTest {

    @Test
    public void test() {
        SynchronizedInteger synchronizedInteger = new SynchronizedInteger();
        UnSynchronizedInteger unSynchronizedInteger = new UnSynchronizedInteger();
        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        int i = 0;
        while (i < 15) {
            Task task = new Task(synchronizedInteger);
//            UnTask task = new UnTask(unSynchronizedInteger);
            pool.submit(task);
            i++;
        }
        pool.shutdown();
        while (true) {
            if (pool.isTerminated()) {
                break;
            }
        }
    }
}
