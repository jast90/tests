package cn.jastz.java.concurrency;

/**
 * @author zhiwen
 */
public class UnSynchronizedInteger {
    private int i = 0;

    public void increase() {
        i++;
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("线程：%s,+1;value:%s", Thread.currentThread().getName(), i));
    }

    public void decrease() {
        i--;
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("线程：%s,-1 ;value:%s", Thread.currentThread().getName(), i));
    }

    public int getValue() {
        System.out.println(String.format("线程：%s,get value:%s", Thread.currentThread().getName(), i));
        return i;
    }
}
