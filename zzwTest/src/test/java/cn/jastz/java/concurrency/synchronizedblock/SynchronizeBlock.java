package cn.jastz.java.concurrency.synchronizedblock;

import com.google.common.util.concurrent.Striped;

import java.util.concurrent.locks.Lock;

/**
 * 同步语句块只能同步对象（引用类型），不能同步基本类型（基本类型封装类也是对象，所以可以同步）。
 * 只用是同一个引用才能实现正确的同步。
 * 如何根据基本类型获取获取锁？可以通过com.google.common.util.concurrent.Striped来获取。
 *
 * @author zhiwen
 */
public class SynchronizeBlock {
    private static final Striped<Lock> striped = Striped.lazyWeakLock(127);

    public void open(String id) {
        //synchronized块，只能是对象，不能是基本类型，可以是基本类型包装类
        Integer i = 1;
        synchronized (i) {
        }
        synchronized (id) {
            System.out.println(String.format("%s: ready open...", Thread.currentThread().getName()));
            System.out.println(String.format("%s: open...", Thread.currentThread().getName()));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("%s: open done...", Thread.currentThread().getName()));
        }
    }

    public void open(int id) {
        String idStr = "" + id;
        //由于是按每次的idStr不是同一个对象，所以该方法无法实现同步
        open(idStr);
    }

    public void openByLock(int id) {
        Lock lock = striped.get(id);
        try {
            lock.lock();
            System.out.println(String.format("%s: ready open...", Thread.currentThread().getName()));
            System.out.println(String.format("%s: open...", Thread.currentThread().getName()));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("%s: open done...", Thread.currentThread().getName()));
        } finally {
            lock.unlock();
        }
    }
}
