package cn.jastz.java.concurrency;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 验证同步方法是锁对象还是锁类（有没有锁方法一说）？（结果是：锁对象）
 * 从下面的输出结果可以得出：
 * 锁的是对象
 */
public class SynchronizedTest {
    /**
     * 输出结果为：
     * m2 ....
     * m1 .... //在m2中sleep设置的时候后才输出的
     */
    @Test
    public void synchronizedMethodLockObjectTest() {
        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        SynchronizedMethod synchronizedMethod = new SynchronizedMethod();
        pool.submit(() -> {
            synchronizedMethod.m2();
        });

        pool.submit(() -> {
            synchronizedMethod.m1();
        });
        pool.shutdown();
        while (true) {
            if (pool.isTerminated()) {
                break;
            }
        }
    }

    /**
     * 输出结果为：
     * m2 ....
     * m1 ....//几乎是同时输出的
     */
    @Test
    public void synchronizedMethodLockClassTest() {
        ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        SynchronizedMethod synchronizedMethod = new SynchronizedMethod();
        SynchronizedMethod synchronizedMethod1 = new SynchronizedMethod();
        pool.submit(() -> {
            synchronizedMethod1.m2();
        });

        pool.submit(() -> {
            synchronizedMethod.m1();
        });
        pool.shutdown();
        while (true) {
            if (pool.isTerminated()) {
                break;
            }
        }
    }
}
