package cn.jastz.java;

/**
 * volatile 具备两种特性：
 * 1. 对所有线程的可见行
 * 2. 禁止指令重排序
 */
public class VolatileTest {
    private static volatile boolean  complete = false;

    /**
     * 验证 volatile禁止重排序特性
     * 输出的结果始终是：
     * load config
     * load config complete,do something
     * @param args
     */
    public static void main(String[] args) {
        Thread a = new Thread(()->{
            System.out.println("load config");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            complete = true;
        });

        Thread b = new Thread(()->{
            while (!complete){
            }
            System.out.println("load config complete,do something");
        });
        b.start();
        a.start();
        while (Thread.activeCount()<=0){
            break;
        }

    }
}
