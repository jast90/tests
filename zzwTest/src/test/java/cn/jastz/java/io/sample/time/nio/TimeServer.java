package cn.jastz.java.io.sample.time.nio;

/**
 * @author zhiwen
 */
public class TimeServer {
    public static void main(String[] args) {
        MultiplexerTimeServer multiplexerTimeServer = new MultiplexerTimeServer(8080);
        new Thread(multiplexerTimeServer).start();
    }
}
