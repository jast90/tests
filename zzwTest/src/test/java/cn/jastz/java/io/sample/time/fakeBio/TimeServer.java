package cn.jastz.java.io.sample.time.fakeBio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 一、伪BIO 服务端编码实现：
 * 1.创建ServerSocket实例
 * 2.等待客户端连接
 * 3.建立连接，获得Socket
 * 4.通过Socket获取相应的IO流进行通信
 * <p>
 * 二、BIO 实现存在的问题：
 * 客户端数量过多时，会创建大量的线程，线程过多必定会出现问题。
 * <p>
 * 三、解决BIO存在的问题：
 * 通过线程池来时避免创建过多线程
 * <p>
 * 四、伪BIO实现存在的问题：
 * 1.解决了线程数过多的问题
 * 2.因为是BIO实现的，还是会存在阻塞的问题
 *
 * @author zhiwen
 */
public class TimeServer {
    public static void main(String[] args) throws IOException {
        int port = 8080;
        try {
            if (args != null && args.length > 0) {
                port = Integer.valueOf(args[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            Socket socket = null;
            ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            while (true) {
                socket = server.accept();
                executorService.execute(new TimeServerHandler(socket));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                server.close();
                server = null;
            }
        }

    }
}
