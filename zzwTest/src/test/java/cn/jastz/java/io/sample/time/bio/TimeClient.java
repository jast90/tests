package cn.jastz.java.io.sample.time.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 1. BIO Socket客户端：
 * 1）.建立连接，通过IP和port创建 Socket 实例
 * 2）.通信，通过Socket获取IO流发送数据
 * <p>
 * 2.该实现是发送一次通信后就关闭连接。如何实现长连接？
 *
 * @author zhiwen
 */
public class TimeClient implements Runnable {

    public static void main(String[] args) throws IOException {
        int size = 10;
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (int i = 0; i < size; i++) {
            executorService.submit(new TimeClient());
        }
        executorService.shutdown();
        while (executorService.isTerminated()) {
            break;
        }
    }

    @Override
    public void run() {
        int port = 8080;
        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            socket = new Socket("127.0.0.1", port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            out.println("currentTime");
            String resp = in.readLine();
            System.out.println("currentTime is:" + resp);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
