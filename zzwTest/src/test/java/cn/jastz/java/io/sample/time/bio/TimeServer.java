package cn.jastz.java.io.sample.time.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 1. BIO 服务端编码实现：
 * 1）.创建ServerSocket实例
 * 2）.等待客户端连接
 * 3）.建立连接，获得Socket
 * 4）.通过Socket获取相应的IO流进行通信
 * <p>
 * 2. 服务端提供的功能：
 * 1）.为客户端提供服务、通信，通过线程来处理各客户端的通信，一个客户端一个线程
 * 2）.维护客户端信息
 * <p>
 * 3. 存在的问题：
 * 客户端数量过多时，会创建大量的线程，线程过多必定会出现问题
 *
 * @author zhiwen
 */
public class TimeServer {
    public static void main(String[] args) throws IOException {
        int port = 8080;
        try {
            if (args != null && args.length > 0) {
                port = Integer.valueOf(args[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            Socket socket = null;
            while (true) {
                socket = server.accept();
                new Thread(new TimeServerHandler(socket)).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (server != null) {
                server.close();
                server = null;
            }
        }

    }
}
