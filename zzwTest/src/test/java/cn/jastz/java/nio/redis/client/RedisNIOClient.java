package cn.jastz.java.nio.redis.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @author zhiwen
 */
public class RedisNIOClient {

    public static void main(String[] args) throws IOException, InterruptedException {

//        sendCmd(client, "set hello world\r\n");
//        System.out.println(type("hash"));
//        System.out.println(ttl("hello"));
//        System.out.println(get("hello"));
        System.out.println(lrange("list", 0, 1));
    }

    public static SocketChannel getClient() throws IOException {
        InetSocketAddress inetSocketAddress = new InetSocketAddress("35.234.6.85", 6379);
        SocketChannel client = SocketChannel.open(inetSocketAddress);
        return client;
    }

    //TODO
    public static String sendCmd(String cmd) {
        SocketChannel client = null;
        try {
            client = getClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (client == null) {
                return null;
            }
            byte[] data = cmd.getBytes();
            ByteBuffer buffer = ByteBuffer.wrap(data);
            client.write(buffer);
            ByteBuffer resBuffer = ByteBuffer.allocate(256);
            client.read(resBuffer);
            String result = new String(resBuffer.array()).trim();
            RedisReplyMessageType redisReplyMessageType = RedisReplyMessageType.getByStartWith(result.substring(0, 1));
            if (redisReplyMessageType != null) {
//                System.out.println(String.format("%s,结果：%s", redisReplyMessageType.getDesc(), result.substring(1)));
            } else {
                System.out.println("Can't resolve redis reply");
            }
            return result.substring(1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                try {
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public static String type(String key) {
        return sendCmd(String.format("type %s\r\n", key));
    }

    public static String ttl(String key) {
        return sendCmd(String.format("ttl %s\r\n", key));
    }

    public static String get(String key) {
        return sendCmd(String.format("get %s\r\n", key));
    }

    public static String lrange(String key, int start, int stop) {
        return sendCmd(String.format("lrange %s %s %s\r\n", key, start, stop));
    }
}
