package cn.jastz.java.nio.redis.client;

/**
 * @author zhiwen
 */
public enum RedisReplyMessageType {

    SIMPLE_STRINGS("redis响应：简单字符串", "+"),
    ERRORS("redis响应：错误", "-"),
    INTEGERS("redis响应：整形", ":"),
    /**
     * Bulk Strings的值格式为：
     * 以\r\n分隔，
     * 第一个子串是：实际要发送的字节数
     * 第二个子串是：实际数据
     */
    BULK_STRINGS("redis响应：散装字符串", "$"),
    /**
     * ARRAYS的值格式为：
     * 以\r\n分隔，
     * 第一个子串是：返回数组的个数
     * 后面的子串是：字节数+实际数据
     */
    ARRAYS("redis响应：数组", "*");

    private String desc;
    private String startWith;

    RedisReplyMessageType(String desc, String startWith) {
        this.desc = desc;
        this.startWith = startWith;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getStartWith() {
        return startWith;
    }

    public void setStartWith(String startWith) {
        this.startWith = startWith;
    }

    public static RedisReplyMessageType getByStartWith(String startWith) {
        for (RedisReplyMessageType value : values()) {
            if (value.getStartWith().equals(startWith)) {
                return value;
            }
        }
        return null;
    }
}

