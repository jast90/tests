package cn.jastz.java.math;

import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author zhiwen
 */
public class BigDecimalTest {

    /**
     * 抛出：java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result.
     */
    @Test
    public void divideThrowArithmeticException() {
        BigDecimal di = BigDecimal.ONE.divide(new BigDecimal(3)).setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println(di);
    }

    /**
     * 抛出：java.lang.ArithmeticException: Division by zero
     */
    @Test
    public void divideByZero() {
        BigDecimal di = BigDecimal.ONE.divide(BigDecimal.ZERO, 2, BigDecimal.ROUND_HALF_UP);
        System.out.println(di);
    }

    /**
     * 正确使用方式：使用 BigDecimal.divide方法时一定要考虑：1.除数是否为0 2.在使用除法时一定要设置精确度
     */
    @Test
    public void divideCorrectUsage() {
        BigDecimal divisor = BigDecimal.ZERO, dividend = BigDecimal.ONE;
        if (divisor != BigDecimal.ZERO || dividend.compareTo(new BigDecimal(0)) == 0) {
            BigDecimal di = dividend.divide(divisor);
            System.out.println(di);
        } else {
            System.out.println("divisor is zero.");
        }

        divisor = new BigDecimal(3);
        if (divisor != BigDecimal.ZERO) {
            BigDecimal di = dividend.divide(new BigDecimal(3), 12, BigDecimal.ROUND_HALF_UP);
            System.out.println(di);
        } else {
            System.out.println("divisor is zero.");
        }

    }


}
