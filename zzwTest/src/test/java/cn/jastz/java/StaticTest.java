package cn.jastz.java;

/**
 * 非法前向引用实例
 *
 * static语句只能访问定义在他之前的static变量,
 * static访问之后定义的static变量时，会编译不通过，会报出非法前向引用
 */
public class StaticTest {


    static{
        i = 0;
//        System.out.println(i);// 非法前向引用
    }

    static int i = 0;

    public static void main(String[] args) {
        System.out.println(StaticTest.i);
    }
}
