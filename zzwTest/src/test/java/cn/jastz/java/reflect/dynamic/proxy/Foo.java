package cn.jastz.java.reflect.dynamic.proxy;

/**
 * @author zhiwen
 */
public interface Foo {
    void bar();
    void rtest();
    void rtest1();
    void rtest2();
}
