package cn.jastz.java.reflect.dynamic.proxy;

import java.lang.reflect.Proxy;

/**
 * @author zhiwen
 */
public class Main {

    public static void main(String[] args) {
        System.getProperties().put(
                "sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        Foo foo = (Foo) DebugProxy.newInstance(new FooImpl());
        Foo foo2 = (Foo) DebugProxy.newInstance(new FooImpl2());
//        System.out.println("foo:" + foo.getClass().getName());
//        System.out.println("foo 2:" + foo2.getClass().getName());
//        System.out.println("foo 2 invocation:" + Proxy.getInvocationHandler(foo2));
        foo.bar();
        foo2.bar();
    }
}
