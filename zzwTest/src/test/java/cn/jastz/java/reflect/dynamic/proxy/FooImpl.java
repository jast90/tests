package cn.jastz.java.reflect.dynamic.proxy;

/**
 * @author zhiwen
 */
public class FooImpl implements Foo {
    @Override
    public void bar() {
        System.out.println("foo impl 1");
    }

    @Override
    public void rtest() {

    }

    @Override
    public void rtest1() {

    }

    @Override
    public void rtest2() {

    }
}
