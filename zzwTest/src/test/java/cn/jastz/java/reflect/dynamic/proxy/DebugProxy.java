package cn.jastz.java.reflect.dynamic.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author zhiwen
 */
public class DebugProxy implements InvocationHandler {

    private Object object;

    public DebugProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result;
        try {
            System.out.println("before method " + method.getName());
            result = method.invoke(object, args);
        } finally {
            System.out.println("after method " + method.getName());
        }
        return result;
    }

    public static Object newInstance(Object o) {
        return Proxy.newProxyInstance(o.getClass().getClassLoader(), o.getClass().getInterfaces(), new DebugProxy(o));
    }
}
