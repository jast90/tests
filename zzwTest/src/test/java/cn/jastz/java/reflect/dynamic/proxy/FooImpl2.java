package cn.jastz.java.reflect.dynamic.proxy;

/**
 * @author zhiwen
 */
public class FooImpl2 implements Foo {
    @Override
    public void bar() {
        System.out.println("foo impl 2");
    }

    @Override
    public void rtest() {

    }

    @Override
    public void rtest1() {

    }

    @Override
    public void rtest2() {

    }
}
