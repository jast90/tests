package cn.jastz.redis.lettuce;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.lettuce.core.GeoArgs;
import me.jastz.common.amap.AMapTemplate;
import me.jastz.common.json.JsonUtil;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.data.geo.Point;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * GEO 内部存的是sorted set
 *
 * @author zhiwen
 */
public class RedisGEO extends AbstractTest {

    private String amapKey = "aaf82134989a8340917d3c9f5f8f2b4c";
    private AMapTemplate amapTemplate = new AMapTemplate(amapKey);
    private List<String> jiakaoStoreAddress = Lists.newArrayList("宝安大道与海城路交汇处劳动综合楼的德信商务中心,49区上川一路裕华花园甲栋21号（滨海春城）,西乡街道宝源路阳光海花园3栋C107号,桃源居2区6栋104号商铺,流塘路金满堂大厦一楼深港驾校,西乡固戍石街新村三巷一号二楼（固戍地铁A出口）,宝城71区留仙二路59号,30区上川路建安新村17栋105号,建安一路48-5号（三区金融街农业银行旁）".split(","));


    @Test
    public void addJiaKaoAddress() {
        String key = "jiakao:store-address";
        commands.del(key);
        Point point = amapTemplate.opsForGeo().singleGeoCode(amapKey, "广东省深圳市宝安区灵芝园新村48栋");
        commands.geoadd(key, point.getX(), point.getY(), "home");
        for (String storeAddress : jiakaoStoreAddress) {
            point = amapTemplate.opsForGeo().singleGeoCode(amapKey, "广东省深圳市宝安区" + storeAddress);
            commands.geoadd(key, point.getX(), point.getY(), storeAddress);
        }
    }

    @Test
    public void distanceHome() {
        Map<String, Double> distanceMap = Maps.newTreeMap();
        String key = "jiakao:store-address";
        for (String storeAddress : jiakaoStoreAddress) {
            distanceMap.put(storeAddress, commands.geodist(key, storeAddress, "home", GeoArgs.Unit.km));
        }
        Map<String, Double> sortMap = distanceMap.entrySet().stream().sorted(Map.Entry.comparingByValue()).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        System.out.println("驾校宝安区门店 距 广东省深圳市宝安区灵芝园新村48栋的距离（单位 km）");
        System.out.println(JsonUtil.objectToPrettyJson(sortMap));
    }

    @Test
    public void geoadd() {
        String key = "geoadd-key";
        commands.del(key);
        Point youbao = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道9996号松日鼎盛大厦8层");
        Point xinzhouqu = amapTemplate.opsForGeo().singleGeoCode(amapKey, "江西省上饶市信州区");
        System.out.println(youbao);
        System.out.println(xinzhouqu);
        commands.geoadd(key, youbao.getX(), youbao.getY(), "友宝");
        commands.geoadd(key, xinzhouqu.getX(), xinzhouqu.getY(), "信州区");
        Assert.assertTrue(commands.zcard(key) == 2);
    }

    @Test
    public void geohash() {
        String key = "geohash-key";
        commands.del(key);
        Point youbao = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道9996号松日鼎盛大厦8层");
        Point xinzhouqu = amapTemplate.opsForGeo().singleGeoCode(amapKey, "江西省上饶市信州区");
        commands.geoadd(key, youbao.getX(), youbao.getY(), "友宝");
        commands.geoadd(key, xinzhouqu.getX(), xinzhouqu.getY(), "信州区");
        System.out.println(commands.geohash(key, "友宝"));
    }

    @Test
    public void geopos() {
        String key = "geopos-key";
        commands.del(key);
        Point youbao = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道9996号松日鼎盛大厦8层");
        commands.geoadd(key, youbao.getX(), youbao.getY(), "友宝");
        System.out.println(youbao);
        System.out.println(commands.geopos(key, "友宝"));
    }

    @Test
    public void geodist() {
        String key = "geodist-key";
        commands.del(key);
        Point youbao = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道9996号松日鼎盛大厦8层");
        Point xinzhouqu = amapTemplate.opsForGeo().singleGeoCode(amapKey, "江西省上饶市信州区");
        commands.geoadd(key, youbao.getX(), youbao.getY(), "youbao");
        commands.geoadd(key, xinzhouqu.getX(), xinzhouqu.getY(), "xinzhouqu");
        System.out.println(commands.geodist(key, "youbao", "xinzhouqus", GeoArgs.Unit.km));
    }

    @Test
    public void georadius() {
        String key = "georadius-key";
        commands.del(key);
        Point songridingsheng = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道松日鼎盛大厦");
        Point wanlida = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大万利达大厦");
        Point bike = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大比克大厦");
        commands.geoadd(key, songridingsheng.getX(), songridingsheng.getY(), "songridingsheng");
        commands.geoadd(key, bike.getX(), bike.getY(), "bike");
        commands.geoadd(key, wanlida.getX(), wanlida.getY(), "wanlida");
        System.out.println(commands.georadius(key, wanlida.getX(), wanlida.getY(), 2, GeoArgs.Unit.km));
    }

    @Test
    public void georadiusbymember() {
        String key = "georadiusbymember-key";
        commands.del(key);
        Point songridingsheng = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大道松日鼎盛大厦");
        Point wanlida = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大万利达大厦");
        Point bike = amapTemplate.opsForGeo().singleGeoCode(amapKey, "深圳市南山区深南大比克大厦");
        commands.geoadd(key, songridingsheng.getX(), songridingsheng.getY(), "songridingsheng");
        commands.geoadd(key, bike.getX(), bike.getY(), "bike");
        commands.geoadd(key, wanlida.getX(), wanlida.getY(), "wanlida");
        System.out.println(commands.georadiusbymember(key, "songridingsheng", 2, GeoArgs.Unit.km));
    }
}
