package cn.jastz.mongodb.user;

import cn.jastz.mongodb.BaseTest;
import com.mongodb.client.MongoCollection;

/**
 * @author zhiwen
 */
public class UserBaseTest extends BaseTest {
    MongoCollection<User> userMongoCollection;

    public UserBaseTest() {
        defaultMongoDatabase.withCodecRegistry(pojoCodecRegistry);
        userMongoCollection = defaultMongoDatabase.getCollection("user", User.class);
    }


}
