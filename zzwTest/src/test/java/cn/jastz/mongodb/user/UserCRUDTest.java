package cn.jastz.mongodb.user;

import com.google.common.collect.Lists;
import com.mongodb.Block;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import me.jastz.common.json.JsonUtil;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author zhiwen
 */
public class UserCRUDTest extends UserBaseTest {

    /**
     * create
     */

    @Test
    public void addUser() {
        User user = new User("Jast", 22);
        user.setId(new ObjectId());
        userMongoCollection.insertOne(user);
        System.out.println(JsonUtil.objectToPrettyJson(user));
    }

    @Test
    public void addManyUser() {
        List<User> list = Lists.newArrayList();
        for (int i = 20; i > 0; i--) {
            list.add(new User(String.format("User%s", i), 20 + i));
        }
        userMongoCollection.insertMany(list);

    }

    /**
     * Read
     */
    @Test
    public void findAll() {
        userMongoCollection.find().forEach((Block<? super User>) user -> System.out.println(JsonUtil.objectToPrettyJson(user)));
    }

    @Test
    public void findOne() {
        User user = userMongoCollection.find(Filters.eq("name", "Jast")).first();
        System.out.println(JsonUtil.objectToPrettyJson(user));
    }

    @Test
    public void findOneAndUpdate() {
        User user = userMongoCollection.findOneAndUpdate(Filters.eq("name", "Jast"), Updates.combine(Updates.set("name", "Jast Zhang"), Updates.set("age", 26)));
        System.out.println(JsonUtil.objectToPrettyJson(user));
    }

    @Test
    public void findOneAndDelete() {
        User user = userMongoCollection.findOneAndDelete(Filters.eq("name", "Jast Zhang"));
        System.out.println(JsonUtil.objectToPrettyJson(user));
    }

    /**
     * update
     */
    @Test
    public void updateOne() {
        UpdateResult updateResult = userMongoCollection.updateOne(Filters.eq("age", 22), Updates.set("name", "user22"));
        Assert.assertTrue(updateResult.getModifiedCount() > 0);
    }

    /**
     * delete
     */

    @Test
    public void deleteOne() {
        DeleteResult result = userMongoCollection.deleteOne(Filters.eq("age", 21));
        Assert.assertTrue(result.getDeletedCount() > 0);
    }


}
