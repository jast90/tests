package cn.jastz.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.junit.After;

import java.util.Arrays;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;

/**
 * @author zhiwen
 */
public class BaseTest {
    protected MongoClient mongoClient;
    protected MongoDatabase defaultMongoDatabase;
    protected MongoCollection defaultMongoCollection;
    protected Document defaultDocument;
    protected CodecRegistry pojoCodecRegistry;

    public BaseTest() {
        pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        MongoClient mongoClient = new MongoClient("192.168.99.100", MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
        defaultMongoDatabase = mongoClient.getDatabase("defaults_database");
        defaultMongoCollection = defaultMongoDatabase.getCollection("default_collection");
        defaultDocument = new Document("name", "MongoDB")
                .append("type", "database")
                .append("count", "1")
                .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
                .append("info", new Document("x", 203).append("y", 102));
    }

    @After
    public void tearDown() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }
}
