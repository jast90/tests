package cn.jastz.mongodb;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author zhiwen
 */
public class DocumentTest extends BaseTest {

    @Test
    public void insert() {
        defaultMongoCollection.insertOne(defaultDocument);

    }

    @Test
    public void count() {
        Assert.assertTrue(defaultMongoCollection.count() == 1);
    }

    @Test
    public void find() {
        System.out.println(defaultMongoCollection.find());
    }
}
