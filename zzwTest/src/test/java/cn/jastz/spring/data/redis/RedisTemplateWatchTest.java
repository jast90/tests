package cn.jastz.spring.data.redis;

import org.junit.Test;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author zhiwen
 */
public class RedisTemplateWatchTest extends AbstractRedisTemplateTest {

    @Test
    public void test() {
        redisTemplate.opsForValue().set("hello", "123");
        ExecutorService pool = Executors.newCachedThreadPool();
        pool.submit(() -> {
            String key = "hello";
            redisTemplate.watch(key);
            redisTemplate.multi();
            redisTemplate.delete(key);
            redisTemplate.exec();
        });
        pool.submit(() -> {
            String key = "hello";
            redisTemplate.delete(key);
        });
        pool.shutdown();
        while (true) {

        }
    }
}
