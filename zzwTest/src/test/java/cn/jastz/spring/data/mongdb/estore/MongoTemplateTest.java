package cn.jastz.spring.data.mongdb.estore;

import cn.jastz.spring.data.mongodb.estore.ApplicationConfig;
import cn.jastz.spring.data.mongodb.estore.entity.Address;
import cn.jastz.spring.data.mongodb.estore.entity.Customer;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author zhiwen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class MongoTemplateTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void mongoTemplateInsertAndFindById() {
        Customer customer = new Customer();
        customer.setFirstName("Jast2");
        customer.setLastName("Zhang");
        customer.setEmailAddress("337235666@qq.com");
        customer.setAddresses(Sets.newHashSet(new Address("深南大道", "广东深圳", "中国")));
        mongoTemplate.insert(customer);
        Customer customer1 = mongoTemplate.findById(customer.getId(), Customer.class);
        Assert.assertTrue(customer1 != null);
    }
}
