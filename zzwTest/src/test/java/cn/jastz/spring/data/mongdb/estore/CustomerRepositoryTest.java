package cn.jastz.spring.data.mongdb.estore;

import cn.jastz.spring.data.mongodb.estore.ApplicationConfig;
import cn.jastz.spring.data.mongodb.estore.entity.Address;
import cn.jastz.spring.data.mongodb.estore.entity.Customer;
import cn.jastz.spring.data.mongodb.estore.repository.CustomerRepository;
import com.google.common.collect.Sets;
import me.jastz.common.json.JsonUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigInteger;
import java.util.Optional;

/**
 * @author zhiwen
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
public class CustomerRepositoryTest {
    @Autowired
    private CustomerRepository customerRepository;


    @Test
    public void save() {
        Customer customer = new Customer();
        customer.setFirstName("Jast");
        customer.setLastName("Zhang");
        customer.setEmailAddress("337235669@qq.com");
        customer.setAddresses(Sets.newHashSet(new Address("深南大道", "广东深圳", "中国")));
        customer = customerRepository.save(customer);
        System.out.println(customer.getId().longValue());
        Assert.assertTrue(customer.getId() != null);
    }

    @Test
    public void findById() {
        Optional<Customer> customer = customerRepository.findById(new BigInteger("28139070253162582293455897806"));
        Assert.assertTrue(customer.isPresent());
        if (customer.isPresent()) {
            System.out.println(JsonUtil.objectToPrettyJson(customer.get()));
        }
    }

    @Test
    public void findFirstByFirstName() {

        Optional<Customer> customer = customerRepository.findFirstByFirstName("Jast");
        Assert.assertTrue(customer.isPresent());
        if (customer.isPresent()) {
            System.out.println(JsonUtil.objectToPrettyJson(customer.get()));
        }
    }


}
