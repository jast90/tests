package cn.jastz.rabbitmq.action.order;

import cn.jastz.rabbitmq.action.Consumer;
import cn.jastz.rabbitmq.action.Queues;
import me.jastz.common.json.JsonUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 新订单消息(order_create)：扣减库存消息（stock_reduce）
 *
 * @author zhiwen
 */
public class NewOrderConsumer {


    public static void main(String[] args) throws IOException, TimeoutException {
        Consumer consumer = new Consumer("192.168.99.100");
        consumer.consumer(Queues.order_create.name(), Order.class, order -> {
            System.out.println(JsonUtil.objectToPrettyJson(order));
        });
    }

}
