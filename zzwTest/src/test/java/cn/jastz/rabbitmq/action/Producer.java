package cn.jastz.rabbitmq.action;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import me.jastz.common.json.JsonUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class Producer {
    private String queueName;
    private Channel channel;
    private Connection connection;

    public Producer(String queueName, String host) throws IOException, TimeoutException {
        this.queueName = queueName;
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
        connection = connectionFactory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(this.queueName, false, false, false, null);
    }

    public boolean send(Object order) throws IOException {
        boolean result = false;

        channel.confirmSelect();
        channel.basicPublish("", queueName, null, JsonUtil.objectToByteArray(order));
        try {
            if (channel.waitForConfirms()) {
                result = true;
            } else {
                result = send(order);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void close() {

        try {
            if (channel != null) {
                channel.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

    }
}
