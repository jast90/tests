package cn.jastz.rabbitmq.action;

import com.rabbitmq.client.*;
import me.jastz.common.json.JsonUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class Consumer {
    private Channel channel;

    public Consumer(String host) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
        Connection connection = connectionFactory.newConnection();
        channel = connection.createChannel();

    }

    public <T> boolean consumer(String queue, Class<T> tClass, Callback<T> callback) {
        boolean result = false;
        try {
            channel.queueDeclare(queue, false, false, false, null);
            channel.basicConsume(queue, false, new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    T t = JsonUtil.byteArrayToObject(body, tClass);
                    callback.callBack(t);
                    channel.basicAck(envelope.getDeliveryTag(), true);
                }
            });
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
        return result;
    }

    @FunctionalInterface
    public interface Callback<T> {
        void callBack(T t);
    }
}
