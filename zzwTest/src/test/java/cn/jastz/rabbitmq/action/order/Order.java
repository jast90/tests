package cn.jastz.rabbitmq.action.order;

import java.util.List;

/**
 * @author zhiwen
 */
public class Order {
    private long orderId;
    private List<OrderItem> itemList;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public List<OrderItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<OrderItem> itemList) {
        this.itemList = itemList;
    }


}
