package cn.jastz.rabbitmq.action.order;

import cn.jastz.rabbitmq.action.Producer;
import cn.jastz.rabbitmq.action.Queues;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class NewOrderProducer {

    public static void main(String[] args) {
        Order order = new Order();
        Random random = new Random();
        order.setOrderId(random.nextLong());
        List<OrderItem> orderItemList = Lists.newArrayList();
        for (int i = 0; i < 2; i++) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrderItemId(random.nextLong());
            orderItem.setProductId(random.nextLong());
            orderItem.setQty(1);
            orderItem.setOrderId(order.getOrderId());
            orderItemList.add(orderItem);
        }
        order.setItemList(orderItemList);
        boolean msgSent = false;
        Producer producer = null;
        try {
            producer = new Producer(Queues.order_create.name(), "192.168.99.100");
            msgSent = producer.send(order);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (producer != null) {
                producer.close();
            }
        }
        if (msgSent) {
            System.out.println("order success");
        } else {
            System.out.println("order fail");
        }
    }
}
