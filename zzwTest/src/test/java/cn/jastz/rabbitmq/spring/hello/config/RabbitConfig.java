package cn.jastz.rabbitmq.spring.hello.config;

import cn.jastz.rabbitmq.Base;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;


/**
 * @author zhiwen
 */
@Configuration
public class RabbitConfig extends Base {
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host);
        cachingConnectionFactory.setPublisherConfirms(true);
        cachingConnectionFactory.setPublisherReturns(true);
        return cachingConnectionFactory;
    }

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory());
        rabbitTemplate.setMandatory(true);
        rabbitTemplate.setConfirmCallback(confirmCallback());
        return rabbitTemplate;
    }

    @Bean
    public Queue myQueue() {
        return new Queue("myqueue");
    }

    @Bean
    public ConfirmCallbackTestImplementation confirmCallback() {
        return new ConfirmCallbackTestImplementation();
    }

    public static class ConfirmCallbackTestImplementation implements RabbitTemplate.ConfirmCallback {

        private volatile Map<String, Boolean> confirmations = new HashMap<>();
        private volatile HashMap<String, CountDownLatch> expectationLatches = new HashMap<>();

        @Override
        public void confirm(CorrelationData correlationData, boolean success, String s) {
            confirmations.put(correlationData.getId(), success);
            expectationLatches.get(correlationData.getId()).countDown();
        }

        public CountDownLatch expect(String correlationId) {
            CountDownLatch latch = new CountDownLatch(1);
            this.expectationLatches.put(correlationId, latch);
            return latch;
        }

    }
}
