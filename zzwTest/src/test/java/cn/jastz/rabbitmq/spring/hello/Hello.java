package cn.jastz.rabbitmq.spring.hello;

/**
 * @author zhiwen
 */

import cn.jastz.rabbitmq.spring.hello.config.RabbitConfig;
import me.jastz.common.json.JsonUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.support.CorrelationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.UnsupportedEncodingException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RabbitConfig.class)
public class Hello {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    public RabbitConfig.ConfirmCallbackTestImplementation confirmCallback;


    @Test
    public void send() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {

            final int a = i;
            executorService.submit(() -> {
                Object msg = "Hello" + a;
                String id = UUID.randomUUID().toString().replace("-", "");
                CountDownLatch countDownLatch = confirmCallback.expect(id);
                rabbitTemplate.convertAndSend("myqueue", msg, new CorrelationData(id));
                try {
                    if (countDownLatch.await(10, TimeUnit.SECONDS)) {
                        System.out.println(String.format("%s:消息发送成功", Thread.currentThread().getName()));
                    } else {
                        System.out.println(String.format("%s:消息发送失败", Thread.currentThread().getName()));
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
        while (true) {
            if (executorService.isTerminated()) {
                break;
            }
        }

    }

    @Test
    public void receive() {
        Object s = (String) amqpTemplate.receiveAndConvert("myqueue");
        System.out.println(s);
    }
}
