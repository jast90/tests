package cn.jastz.rabbitmq.tutorial.routing;

import cn.jastz.rabbitmq.Base;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class EmitLogDirect extends Base {

    private static final String EXCHANGE_NAME = "direct_logs";

    /**
     * 发送消息：info Hello routing（设置main args[info Hello routing])
     * 发送消息：debug Hello routing（设置main args[debug Hello routing])
     * 发送消息：warning Hello routing（设置main args[warning Hello routing])
     * 发送消息：error Hello routing（设置main args[error Hello routing])
     *
     * @param args
     * @throws IOException
     * @throws TimeoutException
     */
    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        //路由键：routingKey
        String severity = getSeverity(args);
        String message = getMessage(args);
        channel.basicPublish(EXCHANGE_NAME, severity, null, message.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + severity + "':'" + message + "'");

        channel.close();
        connection.close();
    }

    private static String getMessage(String[] args) {
        if (args.length < 2) {
            return "Hello World!";
        }
        return joinStrings(args, " ", 1);
    }

    private static String getSeverity(String[] args) {
        if (args.length < 1) {
            return "info";
        }
        return args[0];
    }
}
