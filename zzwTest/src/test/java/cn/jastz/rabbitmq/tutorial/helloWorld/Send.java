package cn.jastz.rabbitmq.tutorial.helloWorld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Send {


    public static void main(String[] args) throws IOException, TimeoutException {
        String host;
        host = "localhost";
        host = "192.168.99.100";
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(host);
        Connection connection = null;

        connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("Hello", false, false, false, null);
        String message = "Hello World";
        channel.basicPublish("", "Hello", null, message.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();

    }

}
