package cn.jastz.rabbitmq.tutorial.remoteProcedureCall;

import cn.jastz.rabbitmq.Base;

/**
 * @author zhiwen
 */
public class ServerMain extends Base {

    public static void main(String[] args) {
        String rpcRequest = "request_queue";
        RPCServer rpcServer = new RPCServer(rpcRequest, host);
        rpcServer.start();
    }
}
