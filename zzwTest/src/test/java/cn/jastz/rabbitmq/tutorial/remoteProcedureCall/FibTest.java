package cn.jastz.rabbitmq.tutorial.remoteProcedureCall;

import cn.jastz.rabbitmq.Base;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class FibTest extends Base {
    String rpcRequest = "request_queue";
    RPCClient rpcClient;

    @Before
    public void setUp() throws IOException, TimeoutException {
        rpcClient = new RPCClient(rpcRequest, host);
    }

    @Test
    public void fib() {
        try {
            System.out.println(rpcClient.call(7));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
