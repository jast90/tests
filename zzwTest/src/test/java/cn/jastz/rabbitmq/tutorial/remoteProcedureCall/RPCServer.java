package cn.jastz.rabbitmq.tutorial.remoteProcedureCall;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @author zhiwen
 */
public class RPCServer {
    private String rpcQueue;
    private String host;

    public RPCServer(String rpcQueue, String host) {
        this.rpcQueue = rpcQueue;
        this.host = host;
    }

    public void start() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = null;
        try {
            connection = factory.newConnection();
            final Channel channel = connection.createChannel();
            channel.queueDeclare(rpcQueue, false, false, false, null);
            channel.basicQos(1);
            System.out.println(" [x] Awaiting RPC requests");
            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder().correlationId(properties.getCorrelationId()).build();
                    String response = "";
                    String message = new String(body, "UTF-8");
                    try {
                        int n = Integer.parseInt(message);
                        System.out.println(String.format("[.] correlationId：【%s】 fib(" + message + ")", properties.getCorrelationId()));
                        response += fib(n);
                    } catch (Exception e) {
                        System.out.println(" [.] " + e.toString());
                    } finally {
                        channel.basicPublish("", properties.getReplyTo(), replyProps, response.getBytes("UTF-8"));
                        //手动发送 消息消费成功 确认消息
                        channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                    synchronized (this) {
                        this.notify();//释放锁
                    }
                }
            };
            //设置为手动发送确认消息
            channel.basicConsume(rpcQueue, false, consumer);

            while (true) {
                //等待客户端调用

                /**
                 * consumer 被锁就等待
                 */
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private int fib(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fib(n - 1) + fib(n - 2);
    }
}
