package cn.jastz.rabbitmq.tutorial.routing;

import cn.jastz.rabbitmq.Base;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @author zhiwen
 */
public class ReceiveLogDirect extends Base {
    private static final String EXCHANGE_NAME = "direct_logs";

    /**
     * 客户端1：设置main args 参数为【info debug warning error】
     * 客户端2：设置main args 参数为【info】
     *
     * @param argv
     * @throws Exception
     */
    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        String queueName = channel.queueDeclare().getQueue();
        if (argv.length < 1) {
            System.err.println("Usage: ReceiveLogsDirect [info] [warning] [error]");
            System.exit(1);
        }

        //设置多个绑定键
        for (String severity : argv) {
            //绑定键（severity）和路由键一致就能收到相关消息
            channel.queueBind(queueName, EXCHANGE_NAME, severity);
        }

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}
