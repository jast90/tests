package cn.jastz.rabbitmq.tutorial.remoteProcedureCall;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * @author zhiwen
 */
public class RPCClient {
    private String rpcQueue;
    private final Channel channel;
    private String replyQueueName;

    public RPCClient(String rpcQueue, String host) throws IOException, TimeoutException {
        this.rpcQueue = rpcQueue;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        channel = factory.newConnection().createChannel();
        replyQueueName = channel.queueDeclare().getQueue();
        System.out.println(replyQueueName);
        System.out.println(channel.queueDeclare().getQueue());
    }

    public String call(int i) throws IOException, InterruptedException {
        final String corrId = UUID.randomUUID().toString().replace("-", "");
        AMQP.BasicProperties props = new AMQP.BasicProperties.Builder()
                .correlationId(corrId)//客户端编号
                .replyTo(replyQueueName)//服务器响应该客户端的消息所存储的队列（响应消息存储的队列）
                .build();
        channel.basicPublish("", rpcQueue, props, String.valueOf(i).getBytes("UTF-8"));
        final BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(1);
        //消费响应的队列消息以获取正确的响应
        channel.basicConsume(replyQueueName, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                if (properties.getCorrelationId().equals(corrId)) {
                    System.out.println("服务器端给回响应，放到阻塞队列中");
                    blockingQueue.offer(new String(body, "UTF-8"));
                }
            }
        });
        System.out.println("阻塞到获取值");
        return blockingQueue.take();
    }
}
