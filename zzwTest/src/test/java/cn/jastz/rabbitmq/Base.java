package cn.jastz.rabbitmq;

/**
 * @author zhiwen
 */
public class Base {
    public static final String LOCALHOST = "localhost";
    public static final String DOCKER_HOST = "192.168.99.100";

    protected static String host;

    static {
        host = LOCALHOST;
        host = DOCKER_HOST;
    }

    protected static String joinStrings(String[] strings, String delimiter, int startIndex) {
        int length = strings.length;
        if (length == 0) return "";
        if (length < startIndex) return "";
        StringBuilder words = new StringBuilder(strings[startIndex]);
        for (int i = startIndex + 1; i < length; i++) {
            words.append(delimiter).append(strings[i]);
        }
        return words.toString();
    }

    protected static String joinStrings(String[] strings, String delimiter) {
        int length = strings.length;
        if (length == 0) return "";
        StringBuilder words = new StringBuilder(strings[0]);
        for (int i = 1; i < length; i++) {
            words.append(delimiter).append(strings[i]);
        }
        return words.toString();
    }


}
